<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\ClubeController;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::prefix('/clubes')->middleware(['auth'])->name('clube.')->group( function(){
    Route::get('/', [ClubeController::class, 'index'])->name('index');
    Route::get('/create', [ClubeController::class, 'create'])->name('create');
    Route::post('/', [ClubeController::class, 'store'])->name('store');
    Route::put('/{id}', [ClubeController::class, 'update'])->name('update');
    Route::delete('/{id}', [ClubeController::class, 'destroy'])->name('destroy');
    Route::get('/{id}', [ClubeController::class, 'edit'])->name('edit');
});

require __DIR__.'/auth.php';
